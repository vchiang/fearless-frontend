//call the restful API,
//get state data back
//loop through it
    //create option element with value of abbrev and text of name


window.addEventListener('DOMContentLoaded', async () => {

  //this is a GET (?)
  const url = 'http://localhost:8000/api/states/';
  const response = await fetch(url);

  if (response.ok) {
    const data = await response.json();

    const selectTag = document.getElementById("state");
    for (let state of data) {
      const option = document.createElement("option");
      option.value = state.abbreviation;
      option.innerHTML = state.name;
      selectTag.appendChild(option);
    }
  }
    const formTag = document.getElementById("create-location-form");
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      // console.log("need to add data");

      const formData = new FormData(formTag); //get a set of key value pairs using FormData
      const json = JSON.stringify(Object.fromEntries(formData));  //turn those k-v pairs into an object using Object.fromEntries, then turn into JSON string
      console.log(json);


    //this is a POST request that we're sending
    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(locationUrl, fetchConfig);

    if (response.ok) {
      formTag.reset();
      const newLocation = await response.json();
    }

    });

});
