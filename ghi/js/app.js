function createCard(name, description, pictureUrl, location, conf_start, conf_end) {
    return `
      <div class="card shadow-lg mb-3">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-title text-secondary">${location}</h6>
            <p class="card-text">${description}</p>
        </div>
        <div class="card-footer bg-light border-success">${conf_start}-${conf_end}</div>
      </div>
    `;
  }

  function showDanger(message) {
    return `
    <div class="alert alert-danger" role="alert">
        ${message}
    </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad

        const row = document.querySelector(".row");
        const message = "Failed: Unable to fetch response"
        const danger = showDanger(message);
        row.innerHTML = danger;

      } else {
        const data = await response.json();

        const card = document.querySelectorAll(".col")
        let i = 0;
        for (let conference of data.conferences) {

          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);


          if (detailResponse.ok) {
            const details = await detailResponse.json();
            // console.log(details);
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;

            const location = details.conference.location.name;
            const conf_start = new Date(details.conference.starts).toLocaleDateString();
            const conf_end = new Date(details.conference.ends).toLocaleDateString();

            const html = createCard(name, description, pictureUrl, location, conf_start, conf_end);
            card[i % 3].innerHTML += html;
            i ++;
          }
        }

      }
    } catch (e) {
        console.error(e);
      // Figure out what to do if an error is raised
      const row = document.querySelector(".row");
      const message = e;
      const error = showDanger(message);
      row.innerHTML = error;
    }

  });



// OLD - before card template
// window.addEventListener('DOMContentLoaded', async () => {
//     const url = 'http://localhost:8000/api/conferences/';

//     try {
//         const response = await fetch(url);

//         if (!response.ok) {
//         // Figure out what to do when the response is bad
//         } else {
//         const data = await response.json();

//         const conference = data.conferences[0];
//         const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML = conference.name;

//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);


//         if (detailResponse.ok) {
//             const details = await detailResponse.json();
//             console.log(details);

//             const descripTag = document.querySelector('.card-text');
//             descripTag.innerHTML = details["conference"]["description"];

//             const imageTag = document.querySelector('.card-img-top');
//             console.log(details.conference.location.picture_url);
//             imageTag.src = details.conference.location.picture_url;

//         }
//         }
//     } catch (e) {
//         // Figure out what to do if an error is raised
//     }

// });
